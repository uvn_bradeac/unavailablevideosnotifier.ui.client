import * as React from 'react'
import { RouteComponentProps, withRouter } from 'react-router-dom'

import { History } from 'history'

export interface IContextState {
    channelId: string,
    isLoggedIn: boolean,
}

interface IStore {
    history: History,
    state: IContextState,
    logout(): void,
    setChannelId(channelId: string): void,
    setIsLoggedIn(value: boolean): void,
}

const store = {
    history: {} as History,
    state: {} as IContextState,
    logout: () => null,
    setChannelId: (channelId: string) => null,
    setIsLoggedIn: (value: boolean) => null,
} as IStore

const Context = React.createContext<IStore>(store)

export const ContextConsumer = Context.Consumer

class ContextProvider extends React.Component<RouteComponentProps, IContextState> {
    constructor(props) {
        super(props)

        this.state = {
            channelId: '',
            isLoggedIn: false,
        }
    }

    public setChannelId = async(channelId: string) => {
        await this.setState(() => ({ channelId }))
    }

    public setIsLoggedIn = async(value: boolean) => {
        await this.setState(() => ({ isLoggedIn: value }))
    }
    
    public logout = async() => {
        localStorage.removeItem('uvn_token')
        localStorage.removeItem('uvn_id_token')

        await this.setState(() => ({ channelId: '', isLoggedIn: false }))
    }

    public render () {
        return(
            <Context.Provider value={{
                history: this.props.history,
                state: this.state,
                logout: this.logout,
                setChannelId: this.setChannelId,
                setIsLoggedIn: this.setIsLoggedIn,
            }}>
                {this.props.children}
            </Context.Provider>
        )
    }
}

export default withRouter(ContextProvider)