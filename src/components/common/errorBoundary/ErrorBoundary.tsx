import * as React from 'react'

interface IErrorBoundaryState {
    hasError: boolean,
}

export class ErrorBoundary extends React.Component<{}, IErrorBoundaryState> {
    constructor(props) {
        super(props)
        this.state = { hasError: false }
    }
  
    public static getDerivedStateFromError() {
        return { hasError: true }
    }
  
    public render() {
        const { children } = this.props
        const { hasError } = this.state

        if (hasError) {
            return <h1>Oops. Something went wrong. Please try to reload the data or refresh the page.</h1>
        }
  
        return children 
    }
}