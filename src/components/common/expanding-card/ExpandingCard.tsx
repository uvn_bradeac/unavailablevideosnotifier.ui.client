import * as React from 'react'

import { ITrack, ITrackDb } from '../../../utils/interfaces'
import { Card } from './Card'

import './ExpandingCard.scss'

interface IExpandingCardProps extends React.HTMLProps<HTMLElement> {
    dbData: ITrackDb,
    youtubeData: ITrack,
}

interface IExpandingCardState {
    clicked: boolean,
}

export class ExpandingCard extends React.Component<IExpandingCardProps, IExpandingCardState> {
    public constructor(props) {
        super(props)

        this.state = {
            clicked: false,
        }
    }

    private handleClick = () => {
        this.setState((prevState) => ({ clicked: !prevState.clicked }))
    }

    public render() {
        const { className, dbData, youtubeData } = this.props

        const initializedYtData = {
            channelId: '',
            channelTitle: '',
            data: {
                channelId: '',
                channelTitle: '',
                count: 0,
                description: '',
                id: '',
                localized: '',
                playlistId: '',
                playlistName: '',
                position: 0,
                publishedAt: '',
                resourceId: '',
                thumbnails: {
                    medium: {
                        url: '',
                    },
                },
                title: '',
            },
            details: {
                videoId: '',
                videoPublishedAt: '',
            },
            id: '',
            title: '',
        } as ITrack

        let ytData = youtubeData ? youtubeData : initializedYtData

        if (!ytData.data.thumbnails) {
            ytData = {
                ...ytData,
                data: {
                    ...ytData.data,
                    thumbnails: {
                        medium: {
                            url: '',
                        }
                    }
                }
            }
        }

        return (
            <>
                <details className={`card-details ${className}`} >
                    <summary className="card-summary" onClick={this.handleClick}>
                        <Card
                            className={className}
                            clicked={this.state.clicked}
                            playlistId={ytData.data.playlistId}
                            position={ytData.data.position}
                            thumbUrl={ytData.data.thumbnails.medium.url}
                            title={ytData.title}
                            uploader={ytData.channelTitle}
                            isExpandedCard={true}
                        />
                    </summary>
                    { 
                        dbData ?
                            <Card 
                                playlistId={dbData.playlistId}
                                position={dbData.position}
                                thumbUrl={dbData.thumbnail.url}
                                title={dbData.title}
                                uploader={dbData.uploader}
                                isExpandedCard={false}
                            />
                        :
                            <div className="card-details__error--not-saved">
                                This track is not saved to the database
                            </div>
                    }
                </details>
            </>
        )
    }
}