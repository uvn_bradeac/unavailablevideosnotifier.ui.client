import * as React from 'react'

import cn from 'classnames'
import { AlertCircle, Save, Triangle, Youtube } from 'react-feather'

import './Card.scss'

interface ICardProps extends React.HTMLProps<HTMLElement>{
    playlistId: string,
    position: number,
    thumbUrl: string,
    title: string,
    uploader: string,
    clicked?: boolean,
    isExpandedCard?: boolean,
    playlistName?: string,
    videoId?: string,
}

export const Card: React.SFC<ICardProps> = ({
    className, clicked, position, title, uploader, isExpandedCard, thumbUrl,
}) => {
    const iconClass = cn({
        'card--expanding__icons__expand': true,
        'card--expanding__icons__expand--clicked': clicked,
    })

    return (
        <div className="card-container">
            <div className="card__left">
                <div className="card__playlist-position">{position + 1}</div>
                {
                    isExpandedCard ? 
                        clicked && <Youtube className="card__data-source" />
                    :   
                        <Save className="card__data-source" />
                }
            </div>
            <div className="card--expanding">
                <div className="card--expanding__info">
                    <img 
                        className="card--expanding__thumb-wrapper" 
                        src={thumbUrl}
                    />
                    <div className="card--expanding__meta">
                        <div className="card--expanding__meta--track-info">
                            <div className="card--expanding__title">{title}</div>
                            <div className="card--expanding__uploader">Uploader: {uploader}</div>
                        </div>
                    </div>
                    <div className="card--expanding__icons">
                        {className && <AlertCircle className="card--expanding__icons__alert"/>}
                        {isExpandedCard && <Triangle className={iconClass} />}
                    </div>
                    {!clicked && <hr className="card--expanding__hr" />}
                </div>
            </div>
        </div>
    )
}