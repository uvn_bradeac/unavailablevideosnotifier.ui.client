import * as React from 'react'

import './Button.scss'

interface IButtonProps extends React.HTMLProps<HTMLButtonElement> {
    text?: string,
    onClick(): void,
}

export const Button: React.SFC<IButtonProps> = ({ 
    children, className, text, onClick 
}) => {
    return (
        <button className = {className} onClick = {onClick}>
            {text ? text : children}
        </button>
    )
}