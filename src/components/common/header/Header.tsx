import * as React from 'react'

import { LogOut, Menu } from 'react-feather'

import { ContextConsumer } from '../../../store/context'

import './Header.scss'

interface IHeaderProps extends React.HTMLProps<HTMLDivElement> { 
    onMenuClick?: () => void,
}

export const Header: React.SFC<IHeaderProps> = ({ className, onMenuClick }) => (
    <ContextConsumer>
        {({ state: { isLoggedIn }, logout }) => (
            <header className={className}>
                <button
                    className="header__hamburger-button"
                    data-tooltip={true}
                    title="Menu"
                    id="menu"
                    onClick={onMenuClick}
                >
                    <Menu />
                </button>
                <p className="header__title">Unavailable videos notifier</p>
                {isLoggedIn && 
                    <div className="header__buttons">
                        <button
                            className="header__buttons__settings"
                            data-tooltip={true}
                            title="Logout"
                            id="logout"
                            onClick={logout}
                        >
                            <LogOut className="header__buttons__settings__logout" />
                        </button>
                    </div>
                }
            </header>
        )}
    </ContextConsumer>
)