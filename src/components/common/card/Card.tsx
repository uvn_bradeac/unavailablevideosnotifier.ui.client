import * as React from 'react'

import './Card.scss'

interface ICardProps {
    playlistId: string,
    position: number,
    thumbUrl: string,
    title: string,
    uploader: string,
    playlistName?: string,
    videoId?: string,
}

export const Card: React.SFC<ICardProps> = ({
    position, title, uploader, playlistId, playlistName, thumbUrl,
}) => {
    return (
        <div className="card-container">
            <div className="card__left">
                <div className="card__playlist-position">{position + 1}</div>
            </div>
            <a 
                href={`https://youtube.com/playlist?list=${playlistId}`} 
                className="card"
            >
                <div className="card__info">
                    <img 
                        className="card__thumb-wrapper" 
                        src={thumbUrl}
                    />
                    <div className="card__meta">
                        <div className="card__meta--track-info">
                            <div className="card__title">{title}</div>
                            <div className="card__uploader">Uploader: {uploader}</div>
                        </div>
                        <div className="card__meta--playlist-info">
                            <div className="card__playlist-name">
                                Playlist name: {playlistName}
                            </div>
                            <div className="card__playlist-position--mobile">
                                Position in playlist: {position + 1}
                            </div>
                        </div>
                    </div>
                    <hr className="card__hr" />
                </div>
            </a>
        </div>
    )
}