import * as React from 'react'

import './Spinner.scss'

interface ISpinner extends React.HTMLProps<HTMLElement> { }

export const Spinner: React.SFC<ISpinner> = ({ className }) => (
    <div className={className} />
)