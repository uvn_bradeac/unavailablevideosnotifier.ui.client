import * as React from 'react'

import { Button } from '../button'

interface IFloatingButtonsProps extends React.HTMLProps<HTMLButtonElement> {
    onClick(): void,
}

export const FloatingButton: React.SFC<IFloatingButtonsProps> = ({ 
    children, className, onClick 
}) => {
    return (
        <Button className={className} onClick={onClick}>
            {children}
        </Button>
    )
}