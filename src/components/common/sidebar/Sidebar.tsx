import * as React from 'react'

import cn from 'classnames'

import { Home, List, RefreshCw, Save } from 'react-feather'

import { IPlaylist } from '../../../utils/interfaces'
import { Spinner } from '../../common/spinner'

import './Sidebar.scss'

interface ISidebarProps extends React.HTMLProps<HTMLButtonElement> { 
    isLoadingSidebar: boolean,
    onHomeClick: () => void,
    onPlaylistClick: (event: any) => Promise<void>,
    onRefetchClick: () => void,
    onSaveClick: () => void,
    selectedPlaylistId: string,
    youtubePlaylistsData: IPlaylist[],
}

export const Sidebar: React.SFC<ISidebarProps> = ({ 
    className, 
    isLoadingSidebar,
    onPlaylistClick, 
    onHomeClick, 
    onRefetchClick, 
    onSaveClick,
    selectedPlaylistId,
    youtubePlaylistsData,
}) => {
    if (youtubePlaylistsData.length === 0 && !isLoadingSidebar) {
        return (
            <div className={className}>
                Oops. You probably don't have playlists created.
            </div>
        )
    }

    if (!youtubePlaylistsData && !isLoadingSidebar) {
        return (
            <div className={className}>
                Oops. There was an error, please reload the page.
            </div>
        )
    }

    if (youtubePlaylistsData.hasOwnProperty('errorMessage')) {
        const error = youtubePlaylistsData as any

        return (
            <div className={className}>
                {error.errorMessage}
                <br/>
                Please refresh the page or try with another account.
            </div>
        )
    }

    const sidebarListHomeButtonClassname = cn({
        'sidebar__list__button': true,
        'sidebar__list__button--active': selectedPlaylistId === '',
    })

    const homeIconClassName = cn({
        'sidebar__list__button--icon': true,
        'sidebar__list__button--icon--active--home': selectedPlaylistId === '',
    })

    const sidebarListHomeButtonTextClassname = cn({
        'sidebar__list__button--text': true,
        'sidebar__list__button--text--highlighted': selectedPlaylistId === '',
    })

    return (
        <div className={className}>
            <button
                className={sidebarListHomeButtonClassname}
                data-tooltip={true}
                title="Home"
                id="home-button"
                onClick={onHomeClick}
            >
                <Home className={homeIconClassName} />
                <div className={sidebarListHomeButtonTextClassname}>
                    Home
                </div>
            </button>

            <hr className="sidebar__hr" />

            <button
                className="sidebar__list__button"
                data-tooltip={true}
                title="Refetch data"
                id="refetch-button"
                onClick={onRefetchClick}
            >
                <RefreshCw className="sidebar__list__button--icon" />
                <div className="sidebar__list__button--text">
                    Refetch data
                </div>
            </button>
            <button
                className="sidebar__list__button"
                data-tooltip={true}
                title="Save data"
                id="save-button"
                onClick={onSaveClick}
            >
                <Save className="sidebar__list__button--icon" />
                <div className="sidebar__list__button--text">
                    Save data
                </div>
            </button>

            <hr className="sidebar__hr" />

            {
                isLoadingSidebar ?
                    <Spinner className="sidebar__spinner" />
                :
                    youtubePlaylistsData.map((playlist: IPlaylist) => {
                        const sidebarListButtonClassname = cn({
                            'sidebar__list__button': true,
                            'sidebar__list__button--active': selectedPlaylistId === playlist.id,
                            'sidebar__list__button--normal': selectedPlaylistId !== playlist.id,
                        })
                    
                        const iconClassName = cn({
                            'sidebar__list__button--icon': true,
                            'sidebar__list__button--icon--active': selectedPlaylistId === playlist.id,
                        })

                        const sidebarListButtonTextClassname = cn({
                            'sidebar__list__button--text': true,
                            'sidebar__list__button--text--highlighted': selectedPlaylistId === playlist.id,
                        })

                        return (
                            <button 
                                className={sidebarListButtonClassname}
                                data-tooltip={true}
                                title={playlist.data.title}
                                id={playlist.id} 
                                key={playlist.id}       
                                onClick={onPlaylistClick}
                            >
                                <List 
                                    className={iconClassName}
                                    id={playlist.id}
                                />
                                <div 
                                    className={sidebarListButtonTextClassname}
                                    id={playlist.id} 
                                >
                                    {playlist.data.title}
                                </div>
                            </button>
                        )
                    })
            }
        </div>
    )
}