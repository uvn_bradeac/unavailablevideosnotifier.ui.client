import * as React from 'react'

interface IShowDifferences extends React.HTMLProps<HTMLButtonElement> {
    dbTrackTitle?: string,
    youtubeTrackTitle?: string,
}

export const ShowDifferences: React.SFC<IShowDifferences> = ({ 
    className, 
    dbTrackTitle,
    youtubeTrackTitle,
}) => (
    <>
        <div className={className}>
            <div>{youtubeTrackTitle}</div>
            <div>{dbTrackTitle}</div>
        </div>
        <hr/>
    </>
)