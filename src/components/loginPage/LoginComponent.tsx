import * as React from 'react'

import { Header } from '../common/header'
import { Spinner } from '../common/spinner'

import './LoginPage'
interface ICustomWindow extends Window {
    gapi?: any,
}
const customWindow: ICustomWindow = window

interface ILoginComponentProps {
    isSignedIn: boolean,
    onSignIn(googleUser): Promise<void>,
}

export class LoginComponent extends React.Component<ILoginComponentProps, {}> {
    constructor(props) {
        super(props)
    }

    public onSuccess = (googleUser) => {
        if (!googleUser) {
            return false
        }

        this.props.onSignIn(googleUser)

        customWindow.gapi.load('auth2', () => {
            const auth2 = customWindow.gapi.auth2.getAuthInstance()
            auth2.signOut()
        })

        return true
    }
    
    public componentDidMount() {    
        customWindow.gapi.signin2.render('google-signin-button', {
            'longtitle': window.innerWidth > 768 ? true : false,
            'onsuccess': this.props.onSignIn,
            'scope': 'https://www.googleapis.com/auth/youtube.readonly',
            'theme': 'light',
            'width': window.innerWidth > 768 ? 200 : 100,
        })
    }

    public render() {
        const { isSignedIn } = this.props

        return !isSignedIn && !localStorage.getItem('uvn_token') ? 
            (
                <>
                    <Header className="header login-page__header"/>
                    <div className="login-page__container">
                        <div className="login-page__card">
                            <h2 className="login-page__card__heading">Welcome back</h2>
                            <p className="login-page__card__message">
                                Since this is a Youtube playlist management app, the only supported provider is Google
                            </p>
                            <div id='google-signin-button'/>
                            <p className="login-page__card__footer">
                                We won't store any sensible data, just your channel ID, playlists and tracks
                            </p>
                        </div>
                    </div>
                </>
            )
        :
            (
                <>
                    <Header className="header login-page__header"/>
                    <div id='google-signin-button' style={{ visibility: 'hidden' }} />
                    <div className="login-page__spinner-container">
                        <Spinner className="login-page__spinner"/>
                    </div>
                </>
            )
    }
}