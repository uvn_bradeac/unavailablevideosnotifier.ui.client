// tslint:disable:no-console
import * as React from 'react'

import axios from 'axios'

import { ContextConsumer } from '../../store/context'
import { channelIdUrl } from '../../utils/constants'
import { LoginComponent } from './LoginComponent'

import './LoginPage.scss'

interface ILoginProps extends React.HTMLProps<HTMLButtonElement> { }

export const LoginPage: React.SFC<ILoginProps> = () => ( 
    <ContextConsumer>
        {({ state: { isLoggedIn },
            history, 
            setChannelId, 
            setIsLoggedIn 
        }) => {
            const onSignIn = async(googleUser) => {      
                const token = googleUser.Zi.access_token
                const id_token = googleUser.Zi.id_token
                const authorizationHeader = `Bearer ${token}`
                
                const channelIdResponse = await axios.get(channelIdUrl, {
                    headers: { Authorization: authorizationHeader },
                })
                const channelId = channelIdResponse.data.items[0].id

                localStorage.setItem('uvn_token', token)
                localStorage.setItem('uvn_id_token', id_token)
        
                await setChannelId(channelId)
                await setIsLoggedIn(true)
            }

            if (isLoggedIn) {
                history.push('/dashboard')
            }

            return (
                <LoginComponent 
                    isSignedIn={isLoggedIn}
                    onSignIn={onSignIn}
                />
            )
        }}
    </ContextConsumer>
)