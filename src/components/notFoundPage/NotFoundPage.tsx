import * as React from 'react'

import { ContextConsumer } from '../../store/context'

const NotFoundPage: React.SFC<{}> = () => (
    <ContextConsumer>
        {({ history }) => {
            history.push('/')

            return ''
        }}
    </ContextConsumer>
)

export default NotFoundPage