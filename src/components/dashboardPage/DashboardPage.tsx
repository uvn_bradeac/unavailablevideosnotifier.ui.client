import * as React from 'react'

import cn from 'classnames'
import { History } from 'history'

import { Header } from '../../components/common/header'
import { Sidebar } from '../common/sidebar'

import { ContextConsumer, IContextState } from '../../store/context'
import { 
    getDeletedTracksData, 
    getDifferences, 
    refreshYoutube,
    savePlaylistIds,
    saveTracks,
} from '../../utils/data/dataFetching'
import { 
    IDeletedTrackData, 
    IDifferences, 
    IPlaylist, 
    ISaveResponse 
} from '../../utils/interfaces'

import { DashboardBase } from './DashboardBase'
import './DashboardPage.scss'

export interface IDashboardPageProps extends React.HTMLProps<HTMLButtonElement> { 
    history: History,
    globalState: IContextState,
    getDifferences(playlistId: string): void,
}

export interface IDashboardPageState {
    channelId: string,
    deletedTracksData: IDeletedTrackData[],
    differences: IDifferences[],
    error: boolean,
    isLoading: boolean,
    isLoadingSidebar: boolean,
    isSidebarOpened: boolean,
    selectedPlaylistId: string,
    windowWidth: number,
    youtubePlaylistsData: IPlaylist[],
}

export class DashboardPage extends React.Component<IDashboardPageProps, IDashboardPageState> {
    constructor(props) {
        super(props)

        this.state = {
            channelId: '',
            deletedTracksData: [],
            differences: [],
            error: false,
            isLoading: false,
            isLoadingSidebar: false,
            isSidebarOpened: false,
            selectedPlaylistId: '',
            windowWidth: window.innerWidth,
            youtubePlaylistsData: [],
        }
    }

    public base = React.createRef<DashboardBase>()

    private getDifferences = async(playlistId: string) => {
        const differences = await getDifferences(playlistId)

        await this.setState((prevState) => {
            if ((prevState.differences.filter(item => item.playlistId === playlistId).length === 0)) {
                return { 
                    differences: [{ playlistId, differences }, ...prevState.differences]
                }
            }

            const removedPlaylist = prevState.differences.filter(item => item.playlistId !== playlistId)

            return {
                differences: [{ playlistId, differences }, ...removedPlaylist]
            }
        })
    }

    private fetchYoutubeData = async() => {
        const ref = this.base.current as any
        const channelId = ref.getChannelId()

        await this.setState(() => ({
            isLoading: true,
            isLoadingSidebar: true,
        }))

        const youtubePlaylistsData = await refreshYoutube(channelId)

        await this.setState(() => ({
            isLoading: false,
            isLoadingSidebar: false,
            youtubePlaylistsData,
        }))
    }

    private saveData = async(): Promise<boolean> => {
        const { channelId, youtubePlaylistsData } = this.state

		const response = await Promise.all([
            savePlaylistIds(channelId, youtubePlaylistsData), 
            saveTracks(youtubePlaylistsData)
        ]) as ISaveResponse[]
        
        if (response[0].statusCode === 500 || response[1].statusCode === 500) {
            return false
        }

		return true
    }

    private handleMenuOpen = () => {
        this.setState((prevState) => ({ isSidebarOpened: !prevState.isSidebarOpened }))
    }

    private onHomeClick = async() => {
        this.setState(() => ({ 
            isSidebarOpened: false,
            selectedPlaylistId: '',
        }))
    }

    private onPlaylistClick = async(event) => {
        const id = event.target.id

        await this.setState(() => ({
            isSidebarOpened: false, 
            isLoading: true,
            selectedPlaylistId: id,
        }))

        await this.getDifferences(id)

        await this.setState((prevState) => {
            const { selectedPlaylistId } = this.state
            const hasClickedHome = (prevState.selectedPlaylistId === '' && selectedPlaylistId === '')

            return {
                isLoading: false,
                selectedPlaylistId: hasClickedHome ? '' : id,
            }
        })
    }

    private onSave = async() => {      
        this.setState(() => ({ isLoading: true }))

        const response = await this.saveData()

        if (!response) {
            this.setState(() => ({ error: true, isLoading: false, }))
        }

        if (response) {
            this.setState(() => ({ error: false, isLoading: false }))
        }
    }

    public updateWindowDimensions = () => {
        const { windowWidth } = this.state

        this.setState((prevState) => {
            if (windowWidth < 768 && windowWidth > 0) {
                return { 
                    isSidebarOpened: prevState.isSidebarOpened,
                    windowWidth: window.innerWidth,
                } as any
            }

            if (
                !prevState.isSidebarOpened && 
                prevState.windowWidth === 768 && 
                this.state.windowWidth === 767) {
                    return { 
                        isSidebarOpened: false,
                        windowWidth: window.innerWidth,
                    } as any
            }


            if (
                prevState.isSidebarOpened && 
                prevState.windowWidth === 767 && 
                this.state.windowWidth === 768) {
                    return { 
                        isSidebarOpened: false,
                        windowWidth: window.innerWidth,
                    } as any
            }

            return { windowWidth: window.innerWidth }
        })
      }

    public async componentDidMount() {
        window.addEventListener('resize', this.updateWindowDimensions);

        const ref = this.base.current as any
        const channelId = ref.getChannelId()

        await this.setState(() => ({
            isLoading: true,
            isLoadingSidebar: true,
        }))

        const youtubePlaylistsData = await refreshYoutube(channelId)

        await this.setState(() => ({
            isLoadingSidebar: false,
            youtubePlaylistsData,
        }))

        const deletedTracksData = await getDeletedTracksData(channelId)

        await this.setState(() => ({
            deletedTracksData,
            isLoading: false,
        }))
    }

    public componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
      }
    
    public render() {
        const { differences, selectedPlaylistId, windowWidth, youtubePlaylistsData } = this.state

        const openSidebarClass = cn({
            'sidebar': true,
            'sidebar--floating': this.state.isSidebarOpened && (windowWidth < 768 && windowWidth > 0),
            'sidebar--hidden': !this.state.isSidebarOpened && (windowWidth < 768 && windowWidth > 0),
        })

        return (
            <>
                <Header className="header" onMenuClick={this.handleMenuOpen} />
                <Sidebar 
                    className={`${openSidebarClass}`}
                    isLoadingSidebar={this.state.isLoadingSidebar}
                    onPlaylistClick={this.onPlaylistClick}
                    onHomeClick={this.onHomeClick}
                    onRefetchClick={this.fetchYoutubeData}
                    onSaveClick={this.onSave}
                    selectedPlaylistId={selectedPlaylistId}
                    youtubePlaylistsData={youtubePlaylistsData}
                />
                <ContextConsumer>
                    {({ 
                        history,
                        state,
                    }) => (
                        <DashboardBase
                            differences={differences}
                            history={history}
                            localState={this.state}
                            globalState={state}
                            ref={this.base}
                        />
                    )}
                </ContextConsumer>
            </>
        )
    }
}

export default DashboardPage