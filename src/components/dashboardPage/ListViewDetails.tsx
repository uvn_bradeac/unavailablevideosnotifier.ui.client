import * as React from 'react'

import cn from 'classnames'

import { IDifferences } from '../../utils/interfaces'
import { ExpandingCard } from '../common/expanding-card'

interface IListViewDetails extends React.HTMLProps<HTMLElement> {
    differences: IDifferences[],
    playlistId: string,
}

export const ListViewDetails: React.SFC<IListViewDetails> = ({ className, differences, playlistId }) => {      
    let isDifference = cn({})
    const playlistDifferences = differences.find(item => item.playlistId === playlistId)

    if (playlistDifferences) {
        const data = playlistDifferences.differences
        
        return (
            <div className={className}>
            {                    
                data.map(item => {
                    isDifference = cn({
                        'difference': item.hasDifference,
                    })

                    const id = item.youtube ? item.youtube.id : item.db.id

                    if (item.youtube) {
                        return (
                            <ExpandingCard 
                                className={isDifference}
                                key={id}
                                youtubeData={item.youtube}
                                dbData={item.db}
                            />
                        )                        
                    }

                    return
                })
            }
            </div>
        )                
    }

    return <div>No tracks found ...</div>
}