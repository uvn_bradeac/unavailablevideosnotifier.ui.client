import * as React from 'react'

import { History } from 'history'

import { Spinner } from '../common/spinner'
import SimpleViewPage from '../simpleViewPage'
import { ListViewDetails } from './ListViewDetails'

import { IContextState } from '../../store/context'
import { IDifferences } from '../../utils/interfaces'
import { IDashboardPageState } from './DashboardPage'

interface IDashboardBaseProps {
    differences: IDifferences[],
    history: History,
    localState: IDashboardPageState,
    globalState: IContextState
}

export class DashboardBase extends React.Component<IDashboardBaseProps, {}> {   
    public getChannelId() {
        return this.props.globalState.channelId
    }

    public componentDidMount() {
        // dummy call, seems to make this work in DashboardPage
        this.getChannelId()
    }

    public render() {
        const { differences, history, localState, globalState } = this.props
        const { isLoggedIn } = globalState
        const { deletedTracksData: deletedTrackData, error, isLoading, selectedPlaylistId } = localState

        const Dashboard = () => (
            <>
                <div className="listview__container">
                    {
                        isLoading &&
                        <div className="listview__spinner-container">
                            <Spinner className="listview__spinner"/>
                        </div>
                    }
                    { 
                        selectedPlaylistId === '' && !isLoading && 
                        <SimpleViewPage 
                            className="simpleview__container"
                            deletedTracksData={deletedTrackData}
                            isLoading={isLoading}
                            history={history}
                            globalState={globalState}
                        /> 
                    }
                    { 
                        selectedPlaylistId !== '' && !isLoading && 
                        <ListViewDetails 
                            className="listview__main"
                            differences={differences}
                            playlistId={selectedPlaylistId}
                        />
                    }
                </div>
            </>
        )

        if (error) {
            return <div>There was a problem when saving the data. Please try again.</div>
        }

        if (!isLoggedIn) {
            history.push('/')
        }

        return <Dashboard />
    }
}
