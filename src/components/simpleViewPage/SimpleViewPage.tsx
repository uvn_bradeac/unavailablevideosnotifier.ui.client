import * as React from 'react'

import { History } from 'history'

import { IContextState } from '../../store/context'
import { IDeletedTrackData } from '../../utils/interfaces'
import { Card } from '../common/card'
import { Spinner } from '../common/spinner'

import './SimpleViewPage.scss'

interface ISimpleViewPageProps extends React.HTMLProps<HTMLButtonElement> { 
    deletedTracksData: IDeletedTrackData[],
    isLoading: boolean,
    history: History,
    globalState: IContextState,
}

const SimpleViewPage: React.SFC<ISimpleViewPageProps> = ({
    className,
    deletedTracksData,
    isLoading,
    history, 
    globalState,
}) => {
    if (!globalState.isLoggedIn) {
        history.push('/')
    }

    if (isLoading) {
        return (
            <div className={className}>
                <div className="simpleview__spinner-container">
                    <Spinner className="simpleview__spinner"/>                        
                </div>
            </div>
        )
    }

    if (deletedTracksData.hasOwnProperty('errorMessage')) {
        const error = deletedTracksData as any
        return (
            <div className={className}>
                {error.errorMessage}
                <br/>
                Please refresh the page or try with another account.
            </div>
        )
    }

    if (deletedTracksData.length === 0) {
        return <div className={className}>No deleted videos ! Hooray !</div>
    }

    return (
        <div className={className}>
            <h3 className="simpleview__title">Deleted videos</h3>
            <div>
                {
                    deletedTracksData.map((item: IDeletedTrackData) => (
                        <Card
                            key={item.id}
                            playlistId={item.playlistId}
                            playlistName={item.playlistName}
                            position={item.position}
                            thumbUrl={item.thumbnail ? item.thumbnail.url : ''}
                            title={item.title}
                            uploader={item.uploader}
                        />
                    ))
                }
            </div>
        </div>
    )
}

export default SimpleViewPage