export interface IPlaylist {
    id: string,
    data: IData,
    count: number,
}

interface IData {
    channelId: string,
    channelTitle: string,
    count?: number,
    description: string,
    id: string,
    localized?: any,
    playlistId: string,
    playlistName: string,
    position: number,
    publishedAt: string,
    resourceId?: any,
    thumbnails: any,
    title: string,
}

interface IDetails {
    videoId: string,
    videoPublishedAt: string,
}

export interface ITrack {
    channelId: string,
    channelTitle: string,
    data: IData,
    details: IDetails,
    id: string,
    title: string,
}

export interface ITrackDb {
    id: string,
    playlistId: string,
    position: number,
    title: string,
    thumbnail: {
        url: string,
        width: number,
        height: number,
    },
    uploader: string,
}

export interface IDeletedTrackRawData {
    id: string,
    channelId: string,
    playlistId: string,
    position: number,
}

export interface IDeletedTrackData extends ITrackDb {
    errorMessage?: string,
    playlistName: string,
    thumbnail: {
        height: number,
        width: number,
        url: string,
    }
    uploader: string,
}

export interface ISaveResponse {
    body: string,
    statusCode: number,
}

interface IDifference {
    db: ITrackDb,
    hasDifference: boolean,
    youtube: ITrack,
}

export interface IDifferences {
    differences: IDifference[],
    playlistId: string,
}