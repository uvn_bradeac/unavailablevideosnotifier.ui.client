import axios from 'axios'
import {
    deletedTracksDataUrl,
    getDifferencesUrl,
    playlistIdsDbUrl,
	playlistIdsYoutubeUrl,
    playlistNameUrl,
	tracksDbUrl,
	tracksYoutubeUrl,
} from '../constants'
import { 
    IDeletedTrackData, 
    IDeletedTrackRawData,
    IPlaylist, 
    ISaveResponse,
    ITrack, 
    ITrackDb, 
} from '../interfaces'

export async function getPlaylistIdsFromYoutube(channelId: string,): Promise<IPlaylist[]> {
    const response = await axios.get(playlistIdsYoutubeUrl, {
        params: {
            channelid: channelId,
        }
    })

    return response.data
}

export async function getTracksFromYoutube(playlistId: string): Promise<ITrack[]> {
    const response = await axios.get(tracksYoutubeUrl, {
        params: {
            playlistid: playlistId,
        }
    })

    return response.data
}

export async function getPlaylistIdsFromDb(channelId: string): Promise<IPlaylist[]> {
    const response = await axios.get(playlistIdsDbUrl, {
        params: {
            channelid: channelId,
        }
    })

    if (response.data.errorMessage) {
        return []
    }

    return response.data
}

export async function getTracksFromDb(playlistId: string): Promise<ITrackDb[]> {
    const response = await axios.get(tracksDbUrl, {
        params: {
            playlistid: playlistId,
        }
    })

    if (response.data.errorMessage) {
        return []
    }

    return response.data
}

export async function getPlaylistName(channelId: string, playlistId: string): Promise<string> {
	const response = await axios.get(playlistNameUrl, {
		params: {
			channelid: channelId,
			playlistid: playlistId,
        }    
	})

	return response.data
}

export async function getDeletedTracksData(channelId: string): Promise<IDeletedTrackData[]> {
    const deletedTracksRawResponse = await axios.get(deletedTracksDataUrl, {
        params: {
            channelid: channelId,
        }
    })

    if (deletedTracksRawResponse.data.errorMessage) {
        return deletedTracksRawResponse.data as IDeletedTrackData[]
    }
    
    const deletedTracksRaw = deletedTracksRawResponse.data as IDeletedTrackRawData[]
    
    const deletedTracksData = await Promise.all(deletedTracksRaw.map(async (item: IDeletedTrackRawData) => {       
        const deletedTracks = await axios.post(deletedTracksDataUrl, {
            id: item.id,
            playlistId: item.playlistId,
        })

        const playlistName = await getPlaylistName(channelId, deletedTracks.data.playlistId)
        
        return { ...deletedTracks.data, playlistName }
    }))

    return deletedTracksData as IDeletedTrackData[]
}

export async function getDifferences(playlistId: string) {
    const response = await axios.get(getDifferencesUrl, {
        params: {
            playlistid: playlistId,
        }
    })

    return response.data
}

export async function savePlaylistIds(channelId: string, playlistIdsYoutube): Promise<ISaveResponse> {
    const response = await axios.post(`${playlistIdsDbUrl}?channelid=${channelId}`, {
            playlists: playlistIdsYoutube,
        },
    )

    return response.data as ISaveResponse
}

export async function saveTracks(playlistIdsYoutube: IPlaylist[]): Promise<ISaveResponse> {
    const saveStatus = await Promise.all(playlistIdsYoutube.map(async (item: IPlaylist) => {       
        const tracksData = await getTracksFromYoutube(item.id)
        const tracks = tracksData.map((track: ITrack) => {
            const thumbnail = track.data.thumbnails ? track.data.thumbnails.medium : null

            return {
                id: track.id,
                playlistId: track.data.playlistId,
                position: track.data.position,
                title: track.title,
                thumbnail,
                uploader: track.channelTitle,
            }
        })

        const saveStatusResponse = await axios.post(tracksDbUrl, {
            playlist: item.id,
            tracks,
        })

        return saveStatusResponse.data as ISaveResponse
    }))

    const saveStatusResult = saveStatus.find((item: ISaveResponse) => item.statusCode === 500)

    if (saveStatusResult) {
        return {
            statusCode: 500,
            body: 'error',
        }
    }

    return {
        statusCode: 200,
        body: 'success',
    }
}

export async function refreshYoutube(channelId: string) {
    return await getPlaylistIdsFromYoutube(channelId)
}

export async function refreshDb(channelId: string) {
    return await getPlaylistIdsFromDb(channelId)
}