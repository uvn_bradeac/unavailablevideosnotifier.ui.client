import * as React from 'react'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"

import { Spinner } from './components/common/spinner'
import { LoginPage } from './components/loginPage'
import { ContextProvider } from './store'

import './App.scss'

const DashboardPage = React.lazy(() => import ('./components/dashboardPage'))
const NotFoundPage = React.lazy(() => import ('./components/notFoundPage'))

const App: React.SFC<{}> = () => (
	<Router>
		<ContextProvider>
			<React.Suspense fallback={<Spinner className="app__spinner" />}>
                <Switch>
                    <Route exact={true} path='/' component={LoginPage} />
                    <Route exact={true} path='/dashboard' component={DashboardPage} />
                    <Route component={NotFoundPage} />
                </Switch>
			</React.Suspense>
		</ContextProvider>
	</Router>
)

export default App
